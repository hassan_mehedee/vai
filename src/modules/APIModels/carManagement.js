import axios from "axios";
import { apiBaseUrl } from "../../conifg";

// function for getting all the cars
const allCars = async (filter) => {
    try {
        let response = await axios({
            method: "POST",
            url: `${apiBaseUrl}/xxxx/get_all_populated`,
            headers: { "Content-Type": "application/json" },
            data: JSON.stringify(filter),
        });
        return [true, response?.data?.result];
    } catch (error) {
        return [false, error.response];
    }
}

// create new car function
const newCarCreation = async (newCar) => {
    try {
        let response = await axios({
            method: "POST",
            url: `${apiBaseUrl}/xxxx/new`,
            headers: { "Content-Type": "application/json" },
            data: JSON.stringify(newCar),
        });
        return [true, "Car created successfully!"];
    } catch (err) {
        return [false, err?.response?.data];
    }
};

// modify car function
const modifyCar = async (modifiedCar) => {
    try {
        let response = await axios({
            method: "PUT",
            url: `${apiBaseUrl}/xxxx/modify/${modifiedCar?._id}`,
            headers: { "Content-Type": "application/json" },
            data: JSON.stringify(modifiedCar),
        });
        return [true, "Car Modified successfully!"];
    } catch (err) {
        return [false, err?.response?.data];
    }
};

// function for changing status

const statusUpdate = async (id, status) => {
    try {
        let response = await axios({
            method: "PUT",
            url: `${apiBaseUrl}/xxxx/set_status/${id}/${status}`,
            headers: { "Content-Type": "application/json" },
        });
        return [true, response?.data?.result];
    } catch (error) {
        return [false, error?.response];
    }
}
export { allCars, newCarCreation, modifyCar, statusUpdate };