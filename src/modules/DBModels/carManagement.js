const initialState = {
    name: "",
    status: true,
    char1: "",
    char2: "",
    char3: "",
    platechar: "",
    platenumber: "",
    brand: "",
    color: "",
    model: "",
    license: "",
    expdate: new Date(),
    mileage: 0,
    periodicevent: []
}
const secondaryState = {
    eventName: "",
    period: 0,
    unit: "",
    effectiveDate: new Date(),
}

export { secondaryState, initialState };