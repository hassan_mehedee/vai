import { Box, Grid, makeStyles, Paper, TextField, Button, Tooltip, Switch, Chip, Typography, TablePagination, List, ListItem, ListItemAvatar, Avatar, ListItemText, withStyles, IconButton } from '@material-ui/core';
import React, { useState } from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { EnhancedTableHead, getComparator, stableSort } from '../Resources/EnhancedTableHead.js';
import { IoIosPaper, IoIosSearch } from "react-icons/io";
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
// dialog imports
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

const useStyles = makeStyles((theme) => ({
    root: {
        margin: theme.spacing(2)
    },
    paper: {
        padding: theme.spacing(2)
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
    textfieldClass: {
        '& .MuiOutlinedInput-input': {
            '&::-webkit-outer-spin-button, &::-webkit-inner-spin-button': {
                '-webkit-appearance': 'none',
            },
        }
    },
    table: {
        minWidth: 650,
    },
    tableHeadColors: {
        backgroundColor: "#aaa",
    }
}));

function createData(name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein };
}

const rows = [
    createData('Cupcake', 305, 3.7, 67, 4.3),
    createData('Donut', 452, 25.0, 51, 4.9),
    createData('Eclair', 262, 16.0, 24, 6.0),
    createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
    createData('Gingerbread', 356, 16.0, 49, 3.9),
    createData('Honeycomb', 408, 3.2, 87, 6.5),
    createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
    createData('Jelly Bean', 375, 0.0, 94, 0.0),
    createData('KitKat', 518, 26.0, 65, 7.0),
    createData('Lollipop', 392, 0.2, 98, 0.0),
    createData('Marshmallow', 318, 0, 81, 2.0),
    createData('Nougat', 360, 19.0, 9, 37.0),
    createData('Oreo', 437, 18.0, 63, 4.0),
];
const headCells = [
    { id: 'actions', numeric: false, disablePadding: false, label: 'Action', issortable: false },
    { id: 'carName', numeric: false, disablePadding: false, label: 'Name of the Car', issortable: true },
    { id: 'license', numeric: false, disablePadding: false, label: 'License', issortable: true },
    { id: 'alarms', numeric: false, disablePadding: false, label: 'Alarms', issortable: true },
    { id: 'actions', numeric: false, disablePadding: false, label: 'Actions to Take', issortable: true },
    { id: 'status', numeric: false, disablePadding: false, label: 'Status', issortable: true },
];

// dialogue functions and variables

const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

const ViewAllCars = () => {
    const classes = useStyles();
    const [cars, setCars] = useState([]);
    // table states
    const [order, setOrder] = useState('asc');
    const [orderBy, setOrderBy] = useState('');
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(100);
    const [selectedValue, setSelectedValue] = useState({});
    /*------------All States ends--------------- */

    // table functions start
    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };


    // dialoge states


    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };
    // dialog states and handlers end

    // table functions start
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    // table functions ends
    return (
        <Box className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12} md={12}>
                    <Paper elevation={2} className={classes.paper}>
                        <Grid container spacing={2}>
                            {/* search by car name */}
                            <Grid item xs={6} md={3}>
                                <TextField
                                    // value={searchString.name}
                                    // onChange={(e) => setSearchString({ ...searchString, name: e.target.value, nameisused: true })}
                                    fullWidth
                                    size="small"
                                    label="Search by Car Name"
                                    variant="outlined"
                                    id="outlined-basic"
                                />
                            </Grid>
                            {/* search by license */}
                            <Grid item xs={6} md={3}>
                                <TextField
                                    // value={searchString.name}
                                    // onChange={(e) => setSearchString({ ...searchString, name: e.target.value, nameisused: true })}
                                    fullWidth
                                    size="small"
                                    label="Search by License"
                                    variant="outlined"
                                    id="outlined-basic"
                                />
                            </Grid>
                            {/* license plate numbers */}
                            <Grid item xs={6} md={3}>
                                <TextField
                                    // value={searchString.name}
                                    // onChange={(e) => setSearchString({ ...searchString, name: e.target.value, nameisused: true })}
                                    fullWidth
                                    size="small"
                                    label="Search by Plate Number"
                                    variant="outlined"
                                    id="outlined-basic"
                                />
                            </Grid>
                            {/* license plate characters */}
                            <Grid item xs={6} md={3} style={{ display: 'flex' }}>
                                <TextField
                                    // value={searchString.name}
                                    // onChange={(e) => setSearchString({ ...searchString, name: e.target.value, nameisused: true })}
                                    style={{ marginRight: "0.5rem" }}
                                    fullWidth
                                    size="small"
                                    label="Char 1"
                                    variant="outlined"
                                    id="outlined-basic"
                                />
                                <TextField
                                    // value={searchString.name}
                                    // onChange={(e) => setSearchString({ ...searchString, name: e.target.value, nameisused: true })}
                                    style={{ marginRight: "0.5rem" }}
                                    fullWidth
                                    size="small"
                                    label="Char 2"
                                    variant="outlined"
                                    id="outlined-basic"
                                />
                                <TextField
                                    // value={searchString.name}
                                    // onChange={(e) => setSearchString({ ...searchString, name: e.target.value, nameisused: true })}
                                    fullWidth
                                    size="small"
                                    label="Char 3"
                                    variant="outlined"
                                    id="outlined-basic"
                                />
                            </Grid>
                            {/* brand search field */}
                            <Grid item xs={6} md={3}>
                                <Autocomplete
                                    fullWidth
                                    disablePortal
                                    id="combo-box-demo"
                                    // value={searchString?.status ? "Active" : "Inactive"}
                                    options={["Active", "Inactive"]}
                                    getOptionLabel={(option) => option}
                                    renderInput={(params) => (
                                        <TextField {...params} variant="outlined" label="Search by Brand" />
                                    )}
                                    size="small"
                                // onChange={(e, newValue) => {
                                //     setSearchString({ ...searchString, status: newValue === "Active" ? true : false, statusisused: Boolean(newValue) });
                                // }}
                                />
                            </Grid>
                            {/* color search field */}
                            <Grid item xs={6} md={3}>
                                <Autocomplete
                                    fullWidth
                                    disablePortal
                                    id="combo-box-demo"
                                    // value={searchString?.status ? "Active" : "Inactive"}
                                    options={["Active", "Inactive"]}
                                    getOptionLabel={(option) => option}
                                    renderInput={(params) => (
                                        <TextField {...params} variant="outlined" label="Search by Colors" />
                                    )}
                                    size="small"
                                // onChange={(e, newValue) => {
                                //     setSearchString({ ...searchString, status: newValue === "Active" ? true : false, statusisused: Boolean(newValue) });
                                // }}
                                />
                            </Grid>
                            {/* search by model name */}
                            <Grid item xs={6} md={3}>
                                <Autocomplete
                                    fullWidth
                                    disablePortal
                                    id="combo-box-demo"
                                    // value={searchString?.status ? "Active" : "Inactive"}
                                    options={["Active", "Inactive"]}
                                    getOptionLabel={(option) => option}
                                    renderInput={(params) => (
                                        <TextField {...params} variant="outlined" label="Search by Models" />
                                    )}
                                    size="small"
                                // onChange={(e, newValue) => {
                                //     setSearchString({ ...searchString, status: newValue === "Active" ? true : false, statusisused: Boolean(newValue) });
                                // }}
                                />
                            </Grid>
                            {/* search by status */}
                            <Grid item xs={6} md={3}>
                                <Autocomplete
                                    fullWidth
                                    disablePortal
                                    id="combo-box-demo"
                                    // value={searchString?.status ? "Active" : "Inactive"}
                                    options={["Active", "Inactive"]}
                                    getOptionLabel={(option) => option}
                                    renderInput={(params) => (
                                        <TextField {...params} variant="outlined" label="Search by Status" />
                                    )}
                                    size="small"
                                // onChange={(e, newValue) => {
                                //     setSearchString({ ...searchString, status: newValue === "Active" ? true : false, statusisused: Boolean(newValue) });
                                // }}
                                />
                            </Grid>
                            {/* search buttons */}
                            <Grid item xs={3} md={2}>
                                <Tooltip title="Search Campaign">
                                    <Button
                                        style={{ marginRight: "0.5rem" }}
                                        // onClick={() => search()}
                                        variant="contained"
                                        color="primary"
                                    >
                                        <IoIosSearch fontSize="1.5rem" />
                                    </Button>
                                </Tooltip>
                                <Button
                                    // onClick={() => clearSearch()}
                                    variant="contained"
                                    sx={{ backgroundColor: "secondary" }}
                                    size="medium"
                                >
                                    X
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
                {/* table grid */}
                {/* table grid starts from here */}
                <Grid item={12} md={12}>
                    <Paper>
                        <TableContainer>
                            <Table
                                aria-labelledby="tableTitle"
                                size='small'
                                aria-label="enhanced table"
                            >
                                <EnhancedTableHead
                                    order={order}
                                    orderBy={orderBy}
                                    onRequestSort={handleRequestSort}
                                    headCells={headCells}
                                />
                                <TableBody>
                                    {stableSort(rows, getComparator(order, orderBy))
                                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                        .map((row, index) => {
                                            return (
                                                <TableRow
                                                    hover
                                                    role="checkbox"
                                                    tabIndex={-1}
                                                    key={index}
                                                >
                                                    <TableCell>
                                                        <Box
                                                            style={{
                                                                display: "flex",
                                                                alignItems: "center",
                                                            }}
                                                        >

                                                            <Tooltip title="Manage Fixed Asset">
                                                                <Button
                                                                    // onClick={() => {
                                                                    //     setValue(0);
                                                                    //     setSentAsset(row);
                                                                    // }}
                                                                    color="primary"
                                                                    style={{ marginRight: 0.5 }}
                                                                    variant="contained"
                                                                    size="small"
                                                                >
                                                                    <IoIosPaper />
                                                                </Button>
                                                            </Tooltip>

                                                            <Switch

                                                                size="small"
                                                                color="primary"
                                                                checked={row?.status}
                                                            // onChange={(e) => {
                                                            //     console.log(e.target.checked)
                                                            //     handleStatusChange(row?._id, e.target.checked)
                                                            // }
                                                            // }
                                                            />
                                                        </Box>
                                                    </TableCell>
                                                    <TableCell align="left" component="th" scope="row">
                                                        {row?.name}
                                                    </TableCell>
                                                    <TableCell align="left" style={{ backgroundColor: "#b3e5fc", fontWeight: "bold" }}>{row?.calories}</TableCell>
                                                    <TableCell align="left">
                                                        <Button color="primary" onClick={handleClickOpen}>
                                                            <Chip
                                                                label={row?.fat}

                                                                color="primary"
                                                                size="small"
                                                                onClick={() => {

                                                                }}
                                                            />
                                                        </Button>
                                                    </TableCell>
                                                    <TableCell align="left">
                                                        <Chip
                                                            label={row?.carbs}

                                                            color="secondary"
                                                            size="small"
                                                            onClick={() => {

                                                            }}
                                                        />

                                                    </TableCell>
                                                    <TableCell align="left" style={{ backgroundColor: "#4db6ac", fontWeight: "bold" }}>{row?.protein}
                                                    </TableCell>
                                                </TableRow>
                                            );
                                        })}
                                </TableBody>
                            </Table>
                        </TableContainer>
                        <TablePagination
                            rowsPerPageOptions={[10, 20, 50]}
                            component="div"
                            count={cars.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                        />
                    </Paper>
                </Grid>
            </Grid>

            {/* dialog */}
            <div>
                <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
                    <DialogTitle id="customized-dialog-title" onClose={handleClose}>
                        Modal title
                    </DialogTitle>
                    <DialogContent dividers>
                        <Typography gutterBottom>
                            Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis
                            in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
                        </Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={handleClose} color="primary">
                            Save changes
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        </Box>
    );
};

export default ViewAllCars;