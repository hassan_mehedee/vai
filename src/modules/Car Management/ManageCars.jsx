import { Box, Button, FormControlLabel, Grid, makeStyles, Paper, Switch, TextField, Typography, Tooltip, Input, Hidden } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker, } from '@material-ui/pickers';
import 'date-fns';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { IoIosPaper, } from "react-icons/io";
import { IoAddCircleOutline, IoTrash } from "react-icons/io5";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { initialState, secondaryState } from '../DBModels/carManagement';
import SuccessFailureAlert from '../Resources/SuccessFailureAlert';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import PhotoCamera from '@material-ui/icons/PhotoCamera';

// component basic styles MUI
const useStyles = makeStyles((theme) => ({
    root: {
        margin: theme.spacing(2)
    },
    paper: {
        padding: theme.spacing(2)
    },
    button: {
        marginRight: theme.spacing(1)
    },
}));

// variables for disabling buttons on the top
const viewModeView = "view";
const viewModeEdit = "edit";
const viewModeSave = "save";
const viewModeNew = "new";


// table functions and data


// table functions and data ends
const ManageCars = () => {
    /* ---------start of states ----------*/
    const [rows, setRows] = useState([]);
    const [car, setCar] = useState(initialState);
    const [image, setImage] = useState("");
    const eventArr = [...rows];
    const [mileError, setMileError] = useState(1);
    const [periodEvent, setPeriodEvent] = useState(secondaryState);
    const [viewMode, setViewMode] = useState(viewModeView);
    const classes = useStyles();
    // success failure alert states
    const [openSuccessAlert, setOpenSuccessAlert] = useState(false);
    const [openFailureAlert, setOpenFailureAlert] = useState(false);
    const [message, setMessage] = useState("");
    const [modify, setModify] = useState(-1);
    /* ---------end of states ----------*/

    /*--------------------handler functions area start----------------------*/
    // success and failure message
    const openFailure = (message) => {
        setMessage("");
        setOpenSuccessAlert(false);
        setOpenFailureAlert(true);
        setMessage(message);
    };
    const openSuccess = (message) => {
        setMessage("");
        setOpenSuccessAlert(true);
        setOpenFailureAlert(false);
        setMessage(message);
    };

    // convert image to base64 string 
    const convertToBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);
            fileReader.onload = () => {
                resolve(fileReader.result);
            };
            fileReader.onerror = (error) => {
                reject(error);
            };
        });
    };
    const handleFileUpload = async (e) => {
        const file = e.target.files[0];
        const base64 = await convertToBase64(file);
        // setPostImage({ ...postImage, myFile: base64 });
        console.log("base64 image string", base64);
    };
    // validation
    const validateCar = () => {
        car.platechar = car?.char1 + car?.char2 + car?.char3;
        if (!car?.name) return "There must be a car name";
        if (car?.platenumber.length < 3) return "Plate number must be at least 3 characters long!";
        if (car?.platechar.length < 2) return "Plate Character must be at least 2 characters long!";
        if (!car?.mileage) return "Mileage cannot be 0!"
        return "";
    };

    // event modification handler for passing the index of the selected event
    // const modifyEvent = (index) => {
    //     let elementOrder = (Number(index));
    //     return elementOrder;
    // }
    // clear function 
    const clear = () => {
        setMileError(1);
        setCar(initialState);
        setViewMode(viewModeView);
        setRows([]);
        setPeriodEvent(secondaryState);
    }

    // alert clear function
    const clearAlert = () => {
        setOpenSuccessAlert(false);
        setOpenFailureAlert(false);
        setMessage("");
    }
    const createCar = () => {
        car.platechar = car?.char1 + car?.char2 + car?.char3;
        car.periodicevent = rows;
        console.log(car);
        openSuccess("Successfully Logged");
    }

    const saveCar = () => {
        let err = validateCar();
        if (err !== "") {
            openFailure(err);
            return;
        } else {
            createCar();
            setCar(initialState);
            setViewMode(viewModeView);
            setRows([]);
            setPeriodEvent(secondaryState);
        }
    }

    // console.log(rows);
    // const addToRow = () => {
    //     periodEvent
    // }
    /*--------------------handler functions area end------------------------*/

    // to load data on page refresh or any state update
    // useEffect(() => {

    // }, [rows])
    return (
        <Box className={classes.root}>
            <Grid container spacing={2}>
                <SuccessFailureAlert
                    openSuccessAlert={openSuccessAlert}
                    message={message}
                    openFailureAlert={openFailureAlert}
                    setopenSuccessAlert={setOpenSuccessAlert}
                    setopenFailureAlert={setOpenFailureAlert}
                />
                {/* grid item for buttons starts*/}
                <Grid item xs={12} md={12} lg={12}>
                    <Button
                        size='small'
                        onClick={() => {
                            setViewMode(viewModeNew)
                        }}
                        disabled={viewMode !== viewModeView}
                        className={classes.button}
                        variant="contained"
                        color="secondary">
                        CREATE NEW
                    </Button>
                    <Button size='small'
                        color="secondary"
                        onClick={() => setViewMode(viewModeSave)}
                        className={classes.button}
                        variant="contained"
                        disabled={viewMode !== viewModeEdit}
                    >
                        EDIT
                    </Button>
                    <Button size='small'
                        color="primary"
                        onClick={() => saveCar()}
                        className={classes.button}
                        variant="contained"
                        disabled={viewMode === viewModeView || viewMode === viewModeEdit}
                    >
                        SAVE
                    </Button>
                    <Button className={classes.button}
                        size='small'
                        onClick={() => {
                            clear();
                            clearAlert();
                        }}
                        variant="contained">
                        CANCEL
                    </Button>
                </Grid>
                {/* grid item for buttons ends*/}
                {/* paper grid starts here */}
                <Grid item xs={12}>
                    <Paper elevation={3} className={classes.paper}>
                        <Grid container spacing={1}>
                            {/* status switch */}
                            <Grid
                                item
                                xs={12}
                                style={{ display: "flex", justifyContent: "center" }}
                            >
                                {" "}
                                <FormControlLabel
                                    disabled={
                                        viewMode === viewModeView || viewMode === viewModeEdit
                                    }
                                    control={
                                        <Switch
                                            size="small"
                                            checked={car?.status}
                                            onChange={(e) => {
                                                console.log(e.target.checked);
                                                setCar({ ...car, status: e.target.checked })
                                            }
                                            }
                                            color="primary"
                                        />
                                    }
                                    label="Active"
                                />
                            </Grid>
                            {/* status switch ends */}
                            {/* text and number fields */}
                            <Grid item xs={9}>
                                <Grid container spacing={1}>
                                    {/* 1st row starts */}
                                    {/* car name starts */}
                                    <Grid item xs={5}>
                                        <TextField
                                            // error={!emptyError ? true : false}
                                            // onFocus={() => { setEmptyError(1); }}
                                            // onBlur={() => {
                                            //     if (!currentAsset?.name) {
                                            //         setEmptyError(0);
                                            //     }
                                            // }}
                                            disabled={
                                                viewMode === viewModeView || viewMode === viewModeEdit
                                            }
                                            // helperText={!emptyError ? "name required" : ""}
                                            required
                                            id="outlined-basic"
                                            label="Name"
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            value={car?.name}
                                            onChange={(e) => { setCar({ ...car, name: e.target.value }) }}
                                        />
                                    </Grid>
                                    {/* car name ends */}
                                    {/* license plate characters starts */}
                                    <Grid item xs={3} style={{ display: 'flex' }}>
                                        <TextField
                                            // error={!emptyError ? true : false}
                                            // onFocus={() => { setEmptyError(1); }}
                                            // onBlur={() => {
                                            //     if (!currentAsset?.name) {
                                            //         setEmptyError(0);
                                            //     }
                                            // }}
                                            disabled={
                                                viewMode === viewModeView || viewMode === viewModeEdit
                                            }
                                            // helperText={!emptyError ? "name required" : ""}
                                            style={{ marginRight: "0.5rem" }}
                                            type="text"
                                            inputProps={{ maxLength: 1 }}
                                            id="outlined-basic"
                                            label="Char 1"
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            value={car?.char1}
                                            onChange={(e) => { setCar({ ...car, char1: e.target.value }) }}
                                        />
                                        <TextField
                                            // error={!emptyError ? true : false}
                                            // onFocus={() => { setEmptyError(1); }}
                                            // onBlur={() => {
                                            //     if (!currentAsset?.name) {
                                            //         setEmptyError(0);
                                            //     }
                                            // }}

                                            disabled={
                                                viewMode === viewModeView || viewMode === viewModeEdit
                                            }
                                            // helperText={!emptyError ? "name required" : ""}
                                            inputProps={{ maxLength: 1 }}
                                            style={{ marginRight: "0.5rem" }}
                                            type="text"
                                            id="outlined-basic"
                                            label="Char 2"
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            value={car?.char2}
                                            onChange={(e) => { setCar({ ...car, char2: e.target.value }) }}
                                        />
                                        <TextField
                                            // error={!emptyError ? true : false}
                                            // onFocus={() => { setEmptyError(1); }}
                                            // onBlur={() => {
                                            //     if (!currentAsset?.name) {
                                            //         setEmptyError(0);
                                            //     }
                                            // }}
                                            disabled={
                                                viewMode === viewModeView || viewMode === viewModeEdit
                                            }
                                            // helperText={!emptyError ? "name required" : ""}
                                            inputProps={{ maxLength: 1 }}
                                            type="text"
                                            id="outlined-basic"
                                            label="Char 3"
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            value={car?.char3}
                                            onChange={(e) => { setCar({ ...car, char3: e.target.value }) }}
                                        />
                                    </Grid>
                                    {/* license plate characters starts */}
                                    {/* license plate numbers starts */}
                                    <Grid item xs={4}>
                                        <TextField
                                            // error={!emptyError ? true : false}
                                            // onFocus={() => { setEmptyError(1); }}
                                            // onBlur={() => {
                                            //     if (!currentAsset?.name) {
                                            //         setEmptyError(0);
                                            //     }
                                            // }}
                                            disabled={
                                                viewMode === viewModeView || viewMode === viewModeEdit
                                            }
                                            // helperText={!emptyError ? "name required" : ""}
                                            inputProps={{ inputmode: 'numeric', min: 0, maxLength: 4, minimumLength: 3 }}
                                            required
                                            // type="number"
                                            id="outlined-basic"
                                            label="License Plate Number"
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            value={car?.platenumber}
                                            onChange={(e) => { setCar({ ...car, platenumber: e.target.value }) }}
                                        />
                                    </Grid>
                                    {/* license numbers ends */}
                                    {/* 3rd row ends */}
                                    {/* 1st row ends */}
                                    {/* 2nd row starts */}
                                    {/* brand field starts */}
                                    <Grid item xs={4}>
                                        <TextField
                                            // error={!emptyError ? true : false}
                                            // onFocus={() => { setEmptyError(1); }}
                                            // onBlur={() => {
                                            //     if (!currentAsset?.name) {
                                            //         setEmptyError(0);
                                            //     }
                                            // }}
                                            disabled={
                                                viewMode === viewModeView || viewMode === viewModeEdit
                                            }
                                            // helperText={!emptyError ? "name required" : ""}


                                            id="outlined-basic"
                                            label="Brand"
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            value={car?.brand}
                                            onChange={(e) => { setCar({ ...car, brand: e.target.value }) }}
                                        />
                                    </Grid>
                                    {/* model field starts */}
                                    <Grid item xs={4}>
                                        <TextField
                                            // error={!emptyError ? true : false}
                                            // onFocus={() => { setEmptyError(1); }}
                                            // onBlur={() => {
                                            //     if (!currentAsset?.name) {
                                            //         setEmptyError(0);
                                            //     }
                                            // }}
                                            disabled={
                                                viewMode === viewModeView || viewMode === viewModeEdit
                                            }
                                            // helperText={!emptyError ? "name required" : ""}

                                            id="outlined-basic"
                                            label="Model"
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            value={car?.model}
                                            onChange={(e) => { setCar({ ...car, model: e.target.value }) }}
                                        />
                                    </Grid>
                                    {/* color field starts */}
                                    <Grid item xs={4}>
                                        <TextField
                                            // error={!emptyError ? true : false}
                                            // onFocus={() => { setEmptyError(1); }}
                                            // onBlur={() => {
                                            //     if (!currentAsset?.name) {
                                            //         setEmptyError(0);
                                            //     }
                                            // }}
                                            disabled={
                                                viewMode === viewModeView || viewMode === viewModeEdit
                                            }
                                            // helperText={!emptyError ? "name required" : ""}

                                            id="outlined-basic"
                                            label="Color"
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            value={car?.color}
                                            onChange={(e) => { setCar({ ...car, color: e.target.value }) }}
                                        />
                                    </Grid>
                                    {/* 2nd row ends */}
                                    {/* 3rd row starts */}

                                    {/* license number field starts */}
                                    <Grid item xs={4}>
                                        <TextField
                                            // error={!emptyError ? true : false}
                                            // onFocus={() => { setEmptyError(1); }}
                                            // onBlur={() => {
                                            //     if (!currentAsset?.name) {
                                            //         setEmptyError(0);
                                            //     }
                                            // }}
                                            disabled={
                                                viewMode === viewModeView || viewMode === viewModeEdit
                                            }
                                            // helperText={!emptyError ? "name required" : ""}
                                            id="outlined-basic"
                                            label="License Number"
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            value={car?.license}
                                            onChange={(e) => { setCar({ ...car, license: e.target.value }) }}
                                        />
                                    </Grid>
                                    {/* Expiration date field starts */}
                                    <Grid item xs={4}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                            <KeyboardDatePicker

                                                fullWidth
                                                disabled={viewMode === viewModeView || viewMode === viewModeEdit}
                                                maxDate={new Date()}
                                                size='small'
                                                autoOk
                                                variant="inline"
                                                inputVariant="outlined"
                                                label="Expiration Date"
                                                format="MM/dd/yyyy"
                                                // value={self?.effectivedatetime}
                                                value={car?.expdate}
                                                onChange={(date) => {
                                                    // if (!isNaN(date?.getTime())) {
                                                    //     setSelf({ ...self, effectivedatetime: date })
                                                    // }
                                                    console.log(date);
                                                    setCar({ ...car, expdate: date })
                                                }}
                                                KeyboardButtonProps={{
                                                    'aria-label': 'change date',
                                                }}
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                    {/* mileage field starts */}
                                    <Grid item xs={4}>
                                        <TextField
                                            error={!mileError ? true : false}
                                            onFocus={() => { setMileError(1); }}
                                            onBlur={() => {
                                                if (!car?.mileage) {
                                                    setMileError(0);
                                                }
                                                // getSource(currentAsset?.value);
                                            }}
                                            style={{ backgroundColor: "#e0f7fa" }}
                                            disabled={
                                                viewMode === viewModeView || viewMode === viewModeEdit
                                            }
                                            type="number"
                                            inputProps={{ min: 0 }}
                                            defaultValue={0}
                                            required
                                            id="outlined-basic"
                                            label="KM"
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            // value={!numField ? null : currentAsset?.value}
                                            value={car?.mileage}
                                            helperText={!mileError ? "Mileage cannot be empty and must be greater than 0" : ""}
                                            onChange={(e) => {
                                                console.log(e.target.value);
                                                setCar({ ...car, mileage: parseInt(e.target.value) });
                                            }}
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                            {/* image grid start*/}
                            <Grid item xs={3} >
                                <Grid container spacing={1}>
                                    {/* show image grid */}
                                    {/* upload image field */}
                                    <Grid item xs={12} md={12} style={{ display: 'flex', justifyContent: 'center', }}>
                                        <label htmlFor="upload-photo">
                                            <input
                                                style={{ display: 'none' }}
                                                id="upload-photo"
                                                name="upload-photo"
                                                type="file"
                                                onChange={(e) => {
                                                    setImage(URL.createObjectURL(e.target.files[0]));
                                                    handleFileUpload(e);
                                                }}
                                                disabled={
                                                    viewMode === viewModeView || viewMode === viewModeEdit
                                                }
                                            />

                                            <IconButton
                                                color="primary"
                                                variant="contained"
                                                component="span"
                                                disabled={
                                                    viewMode === viewModeView || viewMode === viewModeEdit
                                                }>
                                                <PhotoCamera />
                                            </IconButton>
                                        </label>
                                        {/* <DropzoneArea onChange={(files) => console.log(files[0])} /> */}


                                    </Grid>
                                    {/* show image and delete icon if there is any image */}
                                    {
                                        image && <Grid item xs={12} md={12} style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                            <img alt="" style={{ width: "7rem", height: "7rem" }} src={image} />
                                            <IconButton
                                                aria-label="delete"
                                                className={classes.margin} color="secondary"
                                                onClick={(e) => {
                                                    setImage("");
                                                    console.log(image);
                                                }}>
                                                <DeleteIcon fontSize="small" />
                                            </IconButton>
                                        </Grid>
                                    }
                                </Grid>
                            </Grid>
                            {/* image grid end*/}

                        </Grid>

                    </Paper>
                </Grid>
                {/* paper grid ends here */}
                <Grid item xs={12}>
                    <Typography variant="h6"><b>Periodic Event</b></Typography>
                </Grid>
                {/* create periodic event area starts */}
                <Grid item xs={12} md={12}>
                    <Paper elevation={3} className={classes.paper}>
                        <Grid container spacing={1}>
                            {/* Event name starts */}
                            <Grid item xs={3} md={3}>
                                <TextField
                                    // error={!emptyError ? true : false}
                                    // onFocus={() => { setEmptyError(1); }}
                                    // onBlur={() => {
                                    //     if (!currentAsset?.name) {
                                    //         setEmptyError(0);
                                    //     }
                                    // }}
                                    disabled={
                                        viewMode === viewModeView || viewMode === viewModeEdit
                                    }
                                    // helperText={!emptyError ? "name required" : ""}

                                    id="outlined-basic"
                                    label="Name"
                                    variant="outlined"
                                    fullWidth
                                    size="small"
                                    value={periodEvent?.eventName}
                                    onChange={(e) => { setPeriodEvent({ ...periodEvent, eventName: e.target.value }) }}
                                />
                            </Grid>
                            {/* Event name ends */}
                            {/* period number field starts */}
                            <Grid item xs={2} md={2}>
                                <TextField
                                    // error={!emptyError ? true : false}
                                    // onFocus={() => { setEmptyError(1); }}
                                    // onBlur={() => {
                                    //     if (!currentAsset?.name) {
                                    //         setEmptyError(0);
                                    //     }
                                    // }}
                                    disabled={
                                        viewMode === viewModeView || viewMode === viewModeEdit
                                    }
                                    // helperText={!emptyError ? "name required" : ""}
                                    defaultValue={0}
                                    inputProps={{ min: 0 }}
                                    type="number"
                                    id="outlined-basic"
                                    label="Period"
                                    variant="outlined"
                                    fullWidth
                                    size="small"
                                    value={periodEvent?.period}
                                    onChange={(e) => { setPeriodEvent({ ...periodEvent, period: parseInt(e.target.value) }) }}
                                />
                            </Grid>
                            {/* period unit field starts */}
                            <Grid item xs={2} md={2}>
                                <Autocomplete
                                    disabled={
                                        viewMode === viewModeView ||
                                        viewMode === viewModeEdit
                                    }
                                    fullWidth
                                    disablePortal
                                    id="combo-box-demo"
                                    // value={currentAsset?.sofref}
                                    options={["Days", "Months", "Years"]}
                                    getOptionLabel={(option) => option}
                                    renderInput={(params) => (
                                        <TextField {...params} variant="outlined" label="Unit" />
                                    )}
                                    size="small"
                                    onChange={(e, newValue) => {
                                        setPeriodEvent({
                                            ...periodEvent,
                                            unit: newValue,
                                        });
                                    }}
                                />
                            </Grid>
                            {/* effective date field starts  */}
                            <Grid item xs={3} md={3}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <KeyboardDatePicker
                                        fullWidth
                                        disabled={viewMode === viewModeView || viewMode === viewModeEdit}
                                        // maxDate={new Date()}
                                        size='small'
                                        autoOk
                                        variant="inline"
                                        inputVariant="outlined"
                                        label="Effective Date"
                                        format="MM/dd/yyyy"
                                        value={periodEvent?.effectiveDate}
                                        onChange={(date) => {
                                            // if (!isNaN(date?.getTime())) {
                                            //     setSelf({ ...self, effectivedatetime: date })
                                            // }
                                            // console.log(date);
                                            setPeriodEvent({ ...periodEvent, effectiveDate: date })
                                        }}
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                            </Grid>
                            {/* add button starts */}
                            <Grid item xs={1}>
                                <Tooltip title="Add Event">
                                    <Button
                                        disabled={viewMode === viewModeView || viewMode === viewModeEdit}
                                        style={{ marginRight: 2 }}
                                        onClick={() => {
                                            if (modify === -1) {
                                                console.log(periodEvent);
                                                setRows([...rows, periodEvent]);
                                                setPeriodEvent(secondaryState);
                                            } else {
                                                // eventArr.splice(modifyEvent(), 1, periodEvent)

                                                // console.log(modifyEvent(), eventArr);
                                                const arr = [...rows];
                                                arr[modify] = periodEvent;
                                                setRows(arr);
                                                setPeriodEvent(secondaryState);
                                                setModify(-1);
                                            }
                                        }}
                                        variant="contained"
                                        color="primary"
                                    >
                                        <IoAddCircleOutline fontSize="1.5rem" />
                                    </Button>
                                </Tooltip>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>

                {/* table grid starts */}
                <Grid item xs={12} md={12}>
                    <TableContainer component={Paper}>
                        <Table size="small" aria-label="a dense table">
                            <TableHead style={{ backgroundColor: "#aaa" }}>
                                <TableRow>
                                    <TableCell><b>Action</b></TableCell>
                                    <TableCell align="right"><b>Event Name</b></TableCell>
                                    <TableCell align="right"><b>Periodic Unit</b></TableCell>
                                    <TableCell align="right"><b>Effective Date</b></TableCell>

                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {rows?.map((row, index) => (
                                    <TableRow key={index}>
                                        <TableCell component="th" scope="row">
                                            <Box style={{ display: "flex", alignItems: "center", }}>
                                                <Tooltip title="Manage Event">
                                                    <Button
                                                        onClick={() => {
                                                            // setValue(0);
                                                            // making sure that it is being modified with modify state
                                                            setModify(index);
                                                            setPeriodEvent(row);
                                                            // modifyEvent(index);
                                                        }}
                                                        color="primary"
                                                        style={{ marginRight: 0.5 }}
                                                        variant="contained"
                                                        size="small"
                                                    >
                                                        <IoIosPaper />
                                                    </Button>
                                                </Tooltip>
                                                <Tooltip title="Delete Event">
                                                    <Button
                                                        onClick={() => {
                                                            // setValue(0);
                                                            // setSentAsset(row);
                                                            // setRows(rows.filter((row, index) =>))
                                                            setRows(rows?.filter((item, i) => index !== i));
                                                        }}
                                                        color="secondary"
                                                        style={{ marginRight: 0.5 }}
                                                        variant="contained"
                                                        size="small"
                                                    >
                                                        <IoTrash />
                                                    </Button>
                                                </Tooltip>
                                            </Box>
                                        </TableCell>
                                        <TableCell align="right">{row?.eventName}</TableCell>
                                        <TableCell align="right">{row?.period}{row?.unit}</TableCell>
                                        <TableCell align="right">{new Date(row?.effectiveDate).toLocaleDateString()}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
                {/* table grid ends */}
            </Grid>
        </Box>
    );
};

export default ManageCars;