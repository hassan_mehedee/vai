import logo from './logo.svg';
import './App.css';
import CarManagementContainer from './modules/Car Management/CarManagementContainer';

function App() {
  return (
    <div>
      <CarManagementContainer />
    </div>
  );
}

export default App;
